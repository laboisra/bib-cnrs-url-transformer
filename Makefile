bib-cnrs-url-transformer.zip:
	zip -r bib-cnrs-url-transformer.zip src

.PHONY: clean
clean:
	rm -f bib-cnrs-url-transformer.zip
