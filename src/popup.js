function BibCnrsUrlTransform (tabs) {
    chrome.storage.sync.get (
        {'institute' : ''},
        function (items) {
            var institute = items.institute;
            document.write ('Trying Bib CNRS connection (' + institute.toUpperCase () + ')')
            if (institute == '') {
                alert ("Please, choose a CNRS Institute in the options page.")
            } else {
                var BibCnrsLogin = "http://" + institute + ".bib.cnrs.fr/login?url=";
                var curtab = tabs [0]
                var newURL = BibCnrsLogin + curtab.url;
                chrome.tabs.update (curtab.id, {url: newURL});
            }
            window.close ()
        });
}

document.addEventListener ('DOMContentLoaded', function() {
    chrome.tabs.query ({active: true, currentWindow: true}, BibCnrsUrlTransform)
}, false);
