// Saves options to chrome.storage.sync.
function save_options () {
  var institute = document.getElementById ('institute').value;
  chrome.storage.sync.set ({
    'institute' : institute,
  }, function () {
    // Update status to let user know options were saved.
    var status = document.getElementById ('status');
    status.textContent = 'Options saved.';
    setTimeout (function () {
      status.textContent = '';
    }, 750);
  });
}

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restore_options () {
  chrome.storage.sync.get ({
    'institute' : '',
  }, function (items) {
    document.getElementById ('institute').value = items.institute;
  });
}

document.addEventListener ('DOMContentLoaded', restore_options);
document.getElementById ('save').addEventListener('click', save_options);
