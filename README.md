# Bib CNRS URL Transformer

## Description

The French CNRS (Centre National de la Recherche Scientifique) has
created a [web service](https://bib.cnrs.fr/) for accessing scientific
bibliographical documents for its associated researchers and
laboratories.  It works by changing the URL of most editing
companies which provide the scientific contents.

This plugin installs a button in the menu bar that allows automatic
transformation of the URL of the currently active tab. Of course, this
will only work if the user is granted access to the current visited
website via bibCNRS. This avoids the need of going to the bibCNRS page
and manually searching for the item visited before.

The plugin can be configured according to the CNRS Institute to which
the user belongs.

It is available for installation at the [Chrome Web Store](https://chrome.google.com/webstore/detail/bib-cnrs-url-transformer/hbhkhlbbcbalkhpgmbleemeemcpaikcb).

## Licensing Conditions

Copyright © 2017, 2018, 2022 Rafael Laboissière

This plugin is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This plugin is distributed in the hope that it will be useful, but
**without any warranty**; without even the implied warranty of
**merchantability** or **fitness for a particular purpose**.  See the
[GNU General Public License](https://www.gnu.org/licenses/#GPL) for
more details.
